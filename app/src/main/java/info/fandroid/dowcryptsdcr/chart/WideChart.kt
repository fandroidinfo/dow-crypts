package info.fandroid.dowcryptsdcr.chart

import android.content.Context
import android.graphics.Color
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import info.fandroid.dowcryptsdcr.MyMarkerView
import info.fandroid.dowcryptsdcr.R
import info.fandroid.dowcryptsdcr.di.App
import info.fandroid.dowcryptsdcr.formatters.PriceValueFormatter
import info.fandroid.dowcryptsdcr.formatters.YearValueFormatter
import kotlinx.android.synthetic.main.fragment_wide_chart.*
import javax.inject.Inject

class WideChart {

    @Inject
    lateinit var context: Context

    @Inject
    lateinit var formatter: YearValueFormatter

    lateinit var chart: LineChart

    lateinit var markerView: MyMarkerView

    private var isScaleEnabled = false




    var x = 0f
    var y = 0f

    var mScale = 1f

    init {
        App.appComponent.inject(this)
    }

    fun initChart(chart: LineChart) {
        this.chart = chart

        // enable description text
        chart.description.isEnabled = false

        // enable touch gestures
        chart.setTouchEnabled(true)

        // enable scaling and dragging
        chart.isDragEnabled = true
        chart.isHighlightPerDragEnabled = true

        chart.setScaleEnabled(true)
        chart.isScaleXEnabled = true
        chart.isScaleYEnabled = false
        chart.setDrawGridBackground(false)

        // if disabled, scaling can be done on x- and y-axis separately
        chart.setPinchZoom(false)



        //chart.setViewPortOffsets(0F, 0F, 0F, 0F)
        chart.maxHighlightDistance = 300F


        // set an alternative background color
        //chart.setBackgroundColor(Color.LTGRAY)


        val data = LineData()
        data.setValueTextColor(Color.BLACK)

        // add empty data
        chart.data = data

        // get the legend (only possible after setting data)
        chart.legend.isEnabled = false

        chart.setDrawMarkers(true)

        markerView = MyMarkerView(context, R.layout.custom_marker_view)

        chart.marker = markerView



        val xl = chart.xAxis
        //xl.setTypeface(tfLight);
        xl.textColor = Color.BLACK
        xl.position = XAxis.XAxisPosition.BOTTOM
        xl.setDrawGridLines(false)
        xl.valueFormatter = formatter
        xl.labelCount = 6
        xl.granularity = 28F
        xl.setAvoidFirstLastClipping(true)
        xl.isEnabled = true

        val leftAxis = chart.axisLeft
        //leftAxis.setTypeface(tfLight);
        leftAxis.textColor = Color.BLACK
//        leftAxis.axisMaximum = 150f
//        leftAxis.axisMinimum = 0f
        val priceValueFormatter = PriceValueFormatter()
        leftAxis.valueFormatter = priceValueFormatter
        leftAxis.setDrawGridLines(true)

        val rightAxis = chart.axisRight
        rightAxis.isEnabled = true
        rightAxis.valueFormatter = priceValueFormatter



    }


    fun addEntry(value: Float, date: String) {
        val data = chart.data

        if (data != null) {

            var set: ILineDataSet? = data.getDataSetByIndex(0)

            if (set == null) {
                set = createSet()
                data.addDataSet(set)
            }

            //Log.d("helloy", "date: $date, float: ${date.toFloatDate()}")



            x = date.toFloat()
            y = value


//            val ddd = Date(x.toFloat().toLong() * 1000).dateToString()
//
//            val dddd = Date(x * 1000).dateToString()
//
//            Log.d("pattern float", "date: $ddd, original: ${x.toFloat()}")
//            Log.d("pattern long", "date: $dddd, original: ${x}")

            data.addEntry(Entry(x, y), 0)
        }
    }

    fun notifyChart() {
        chart.data.notifyDataChanged()

        chart.notifyDataSetChanged()


        val range = 181430004F


        chart.setVisibleXRangeMaximum(range)

        val moveTo = if (isScaleEnabled) x.toFloat() * 1000F else x.toFloat()



        chart.moveViewToX(x.toFloat())

        chart.refreshDrawableState()
        chart.fitScreen()


        chart.zoomOut()
        chart.resetZoom()
    }



    fun setItemsCount(count: Int) {
        formatter.count = count
    }

    fun setScaleEnabled(scale: Boolean) {
        isScaleEnabled = scale
        formatter.isScaleEnabled = scale
        markerView.isScaleEnabled = scale
    }

    fun refresh() {
        chart.scaleX = 1f
        chart.refreshDrawableState()
        chart.fitScreen()

        chart.scaleX = 1f
        chart.zoom(mScale, 1f, x, y)
        chart.moveViewToX(x)

    }


    private fun createSet(): LineDataSet {
        val set = LineDataSet(null, "Dynamic Data")

        set.mode = LineDataSet.Mode.CUBIC_BEZIER
        set.cubicIntensity = 0.2f
        set.setDrawFilled(true)
        set.setDrawCircles(false)
        set.setDrawValues(false)
        set.lineWidth = 1.8f
        set.circleRadius = 4f
        set.setCircleColor(Color.BLACK)
        set.highlightLineWidth = 1.2f
        set.highLightColor = context.resources.getColor(R.color.colorAccent)
        set.color = Color.BLACK
        set.fillColor = Color.BLACK
        set.enableDashedHighlightLine(10f, 5f, 0f)
        set.fillAlpha = 100
        set.setDrawHorizontalHighlightIndicator(true)
        set.setFillFormatter { _, _ ->
            chart.axisLeft.axisMinimum
        }

        return set
    }
}
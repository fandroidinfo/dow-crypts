package info.fandroid.dowcryptsdcr.chart

import android.content.Context
import android.graphics.Color
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import info.fandroid.dowcryptsdcr.HourMarkerView
import info.fandroid.dowcryptsdcr.R
import info.fandroid.dowcryptsdcr.di.App
import info.fandroid.dowcryptsdcr.formatters.HourValueFormatter
import info.fandroid.dowcryptsdcr.formatters.PriceValueFormatter
import info.fandroid.dowcryptsdcr.model.RealmIndex
import io.realm.Realm
import java.util.*
import javax.inject.Inject

class LatestChart {

    @Inject
    lateinit var context: Context

    @Inject
    lateinit var formatter: HourValueFormatter

    lateinit var chart: LineChart

    lateinit var markerView: HourMarkerView

    var x = 0f
    var y = 0f


    init {
        App.appComponent.inject(this)
    }

    fun initChart(chart: LineChart) {
        this.chart = chart

        // enable description text
        chart.description.isEnabled = false

        // enable touch gestures
        chart.setTouchEnabled(true)

        // enable scaling and dragging
        chart.isDragEnabled = true
        chart.isHighlightPerDragEnabled = true

        chart.setScaleEnabled(false)
        chart.setDrawGridBackground(false)
        chart.isDoubleTapToZoomEnabled = false

        // if disabled, scaling can be done on x- and y-axis separately
        chart.setPinchZoom(false)

        chart.maxHighlightDistance = 300f

        //chart.setViewPortOffsets(0F, 0F, 0F, 0F)
        //chart.maxHighlightDistance = 300000000F

        // set an alternative background color
        //chart.setBackgroundColor(Color.LTGRAY)

        val data = LineData()
        data.setValueTextColor(Color.BLACK)

        // add empty data
        chart.data = data

        // get the legend (only possible after setting data)
        chart.legend.isEnabled = false


        markerView = HourMarkerView(context, R.layout.custom_marker_view)
        chart.setDrawMarkers(true)
        chart.marker = markerView




        val xl = chart.xAxis
        //xl.setTypeface(tfLight);
        xl.textColor = Color.BLACK
        xl.position = XAxis.XAxisPosition.BOTTOM
        xl.setDrawGridLines(false)
        xl.valueFormatter = formatter
        xl.labelCount = 6
        xl.granularity = 5f


        xl.setAvoidFirstLastClipping(true)
        xl.isEnabled = true


        val leftAxis = chart.axisLeft
        //leftAxis.setTypeface(tfLight);
        leftAxis.textColor = Color.BLACK
//        leftAxis.axisMaximum = 100f
//        leftAxis.axisMinimum = 0f
        val priceValueFormatter = PriceValueFormatter()
        leftAxis.valueFormatter = priceValueFormatter
        leftAxis.setDrawGridLines(true)

        val index = Realm.getDefaultInstance().where(RealmIndex::class.java).findAll().last()
        val indexes = Realm.getDefaultInstance().where(RealmIndex::class.java).findAll().takeLast(100)
        val max =  indexes.maxBy { it.index!! }
        val min = indexes.minBy { it.index!! }

        leftAxis.axisMaximum = max?.index?.toFloat()!! + 15f
        leftAxis.axisMinimum = min?.index?.toFloat()!! - 15f


        val rightAxis = chart.axisRight
        rightAxis.isEnabled = true
        rightAxis.valueFormatter = priceValueFormatter


        rightAxis.axisMaximum = index?.index?.toFloat()!! + 15f
        rightAxis.axisMinimum = index?.index?.toFloat()!! - 15f
    }


    fun addEntry(value: Float, date: String) {
        val data = chart.data

        if (data != null) {

            var set: ILineDataSet? = data.getDataSetByIndex(0)

            if (set == null) {
                set = createSet()
                data.addDataSet(set)
            }


            x = date.toFloat()
            y = value


            data.addEntry(Entry(x, y), 0)
        }
    }


    fun setItemsCount(count: Int) {
        formatter.count = count
    }


    fun notifyChart() {
        chart.data.notifyDataChanged()

        chart.notifyDataSetChanged()


        val range = 181430004F


        chart.setVisibleXRangeMaximum(range)


        chart.moveViewToX(x.toFloat())

        chart.refreshDrawableState()
        chart.fitScreen()


        chart.zoomOut()
        chart.resetZoom()
    }



    private fun createSet(): LineDataSet {
        val set = LineDataSet(null, "Dynamic Data")

        set.mode = LineDataSet.Mode.CUBIC_BEZIER
        set.cubicIntensity = 0.2f
        set.setDrawFilled(true)
        set.setDrawCircles(false)
        set.setDrawValues(false)
        set.lineWidth = 1.8f
        set.circleRadius = 4f
        set.setCircleColor(Color.BLACK)
        set.highlightLineWidth = 1.2f
        set.highLightColor = context.resources.getColor(R.color.colorAccent)
        set.color = Color.BLACK
        set.fillColor = Color.BLACK
        set.enableDashedHighlightLine(10f, 5f, 0f)
        set.fillAlpha = 100
        set.setDrawHorizontalHighlightIndicator(true)
        set.setFillFormatter { _, _ ->
            chart.axisLeft.axisMinimum
        }
        return set
    }



}
package info.fandroid.dowcryptsdcr.mvp.contract

import android.content.Context
import android.content.Intent
import android.net.Uri
import info.fandroid.dowcryptsdcr.adapter.CurrenciesAdapter

class CurrenciesContract {
    interface View : BaseContract.View {
        fun showIndex(index: String)
        fun showIndexChanges(percent: Double)
        fun addCurrency(currency: CurrenciesAdapter.Currency)
        fun notifyAdapter()
        fun showProgress()
        fun hideProgress()
        fun showErrorMessage(error: String?)
        fun refresh()
    }

    abstract class Presenter: BaseContract.Presenter<View>() {
        abstract fun makeIndex()
        abstract fun refreshIndex()
    }
}
package info.fandroid.dowcryptsdcr.mvp.presenter

import android.content.Context
import android.util.Log
import info.fandroid.dowcryptsdcr.adapter.CurrenciesAdapter
import info.fandroid.dowcryptsdcr.di.App
import info.fandroid.dowcryptsdcr.model.GeckoCoin
import info.fandroid.dowcryptsdcr.model.RealmIndex
import info.fandroid.dowcryptsdcr.mvp.contract.CurrenciesContract
import info.fandroid.dowcryptsdcr.rest.CoinGeckoApi
import info.fandroid.ext.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import java.util.*
import javax.inject.Inject

class CurrenciesPresenter : CurrenciesContract.Presenter() {

    @Inject
    lateinit var geckoApi: CoinGeckoApi

    @Inject
    lateinit var latestChartPresenter: LatestChartPresenter

    @Inject
    lateinit var context: Context

    var index24h = 0f


    init {
        App.appComponent.inject(this)
    }


    override fun makeIndex() {
        view.showProgress()

        latestChartPresenter.loadedIndex = false

        subscribe(geckoApi.getMarketCoins()
            .doOnNext {
                //calculatePriceChanges(it)
            }
            .doOnNext {
                printRating(it)
            }
            .flatMap { Observable.fromIterable(it) }
            .doOnNext {
                view.addCurrency(CurrenciesAdapter.Currency(it))
            }
            .map { it.current_price }
            .reduce { total, next ->
                total + next
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnComplete {
                view.hideProgress()
            }
            .subscribe({
                val index = (it / 100).round(3)
                view.showIndex("$index")


                latestChartPresenter.loadedIndex = true

                if (latestChartPresenter.loaded && latestChartPresenter.loadedIndex) {
                    latestChartPresenter.showPercentage()
                }


//                val realm = Realm.getDefaultInstance()
//
//                val cal = Calendar.getInstance()
//                cal.firstDayOfWeek = Calendar.MONDAY
//
//                cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
//                //cal.add(Calendar.DAY_OF_WEEK, -7)
//
//                Log.d("hellokjh", "cal date: ${cal.timeInMillis.dateToString()}")
//
//                val weekIndex = realm.where(RealmIndex::class.java).equalTo("date", cal.timeInMillis.dateToString()).findFirst()
//
//                if (weekIndex != null) {
//
//
//                    index24h = weekIndex.index?.toFloat()!!
//                    Log.d("hellokjh", "date: ${weekIndex.date}")
//
//                } else {
//                    val query = Realm.getDefaultInstance().where(RealmIndex::class.java).findAll()
//                    val lastIndex = query.get(query.size - 2)!!
//                    Log.d("hellokjh", "date: ${lastIndex.date}")
//                    index24h = lastIndex.index?.toFloat()!!
//                }
//
//                val percent = ((index - index24h) / index) * 100
//                view.showIndexChanges(percent)



                view.notifyAdapter()

                view.hideProgress()
            }, {
                view.showErrorMessage(it.message)
                view.hideProgress()
                it.printStackTrace()
            })
        )
    }


    override fun refreshIndex() {
        view.refresh()
        makeIndex()
    }


//    fun calculatePriceChanges(coins: List<GeckoCoin>): Disposable {
//        return Observable.fromIterable(coins)
//            .map { it.current_price + it.price_change_24h }
//            .reduce { total: Double, next: Double -> total + next }
//            .toObservable()
//            .doOnNext { index24h = it / 100 }
//            .subscribe()
//    }

    fun printRating(coins: List<GeckoCoin>): Disposable {
        return Observable.fromIterable(coins)
            .map { it.id }
            .doOnNext {
                //Log.d("hellok", it)
            }
            .reduce { total: String, next: String -> "$total,$next" }
            .toObservable()
            .doOnNext { Log.d("hellok rating", it) }
            .subscribe()
    }
}
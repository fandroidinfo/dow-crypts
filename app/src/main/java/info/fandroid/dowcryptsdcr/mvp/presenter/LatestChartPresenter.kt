package info.fandroid.dowcryptsdcr.mvp.presenter

import android.os.Handler
import android.os.Looper
import android.util.Log
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import info.fandroid.dowcryptsdcr.formatters.HourValueFormatter
import info.fandroid.dowcryptsdcr.chart.LatestChart
import info.fandroid.dowcryptsdcr.di.App
import info.fandroid.dowcryptsdcr.model.GeckoCoin
import info.fandroid.dowcryptsdcr.model.RealmIndex
import info.fandroid.dowcryptsdcr.mvp.contract.LatestChartContract
import info.fandroid.dowcryptsdcr.rest.CoinGeckoApi
import info.fandroid.ext.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.RealmResults
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import android.support.v4.os.HandlerCompat.postDelayed


class LatestChartPresenter : LatestChartContract.Presenter() {

    @Inject
    lateinit var geckoApi: CoinGeckoApi

    @Inject
    lateinit var formatter: HourValueFormatter

    @Inject
    lateinit var latestChart: LatestChart

    @Inject
    lateinit var mFirebaseRemoteConfig: FirebaseRemoteConfig

    var formatCount = 0

    var coof = 100


    var coins: MutableList<GeckoCoin> = ArrayList()

    val array = ArrayList<Float>()

    var size = 0

    var loading = false
    var loaded = false

    val rating =
        "bitcoin,ethereum,ripple,eos,litecoin,bitcoin-cash,tether,tron,stellar,binancecoin,cardano,bitcoin-cash-sv,monero,iota,dash,neo,ethereum-classic,maker,nem,ontology,zcash,tezos,waves,usd-coin,dogecoin,vechain,holotoken,true-usd,okb,bitcoin-gold,zilliqa,omisego,chainlink,qtum,decred,lisk,augur,basic-attention-token,0x,bitcoin-diamond,zb-token,paxos-standard,bytecoin,pundi-x,nano,icon,bitshares,digibyte,verge,iostoken,siacoin,aeternity,steem,komodo,stratis,dai,status,electroneum,ark,theta-token,golem,factom,maidsafecoin,ardor,mixin,odem,hshare,quark-chain,quant-network,huobi-token,decentraland,populous,loopring,veritaseum,pivx,revain,polymath-network,aelf,reddcoin,gxchain,zcoin,power-ledger,wax,monaco,nexo,bancor,digixdao,monacoin,metaverse-etp,kucoin-shares,ravencoin,wanchain,aion,elastos,gas,singularitynet,tenx,zencash,enjincoin,waltonchain"

    var firebaseRating: List<String>

    var fetched = false
    var loadedIndex = false

    lateinit var mRealm: Realm

    val latestDate = 0


    init {
        App.appComponent.inject(this)

        coins = ArrayList()

        firebaseRating = ArrayList()

        mRealm = Realm.getDefaultInstance()




        for (i in 1..167) {
            array.add(0f)
        }
    }


    override fun makeChart(scaleEnabled: Boolean, distinct: WideChartPresenter.Distinct) {

        mRealm = Realm.getDefaultInstance()
        mRealm.refresh()



        mRealm.executeTransaction {

            val query = mRealm.where(RealmIndex::class.java).findAll()


            size = query.size


            Observable.fromIterable(query)
                .mapWithIndex()
                .filter { it.index() > query.size - 30 }

                .toList()
                .toObservable()
                .doOnNext {

                    formatter.count = it!!.size
                    latestChart.markerView.initList()
                    latestChart.formatter.initList()
                }
                .flatMap { Observable.fromIterable(it) }

                .doOnNext {
                    view.addEntryToChart(it?.value()?.index?.toFloat()!!, it.index().toString())
                }
                .doOnComplete {
                    loadLatest()
                    latestChart.notifyChart()
                    //view.hideProgress()
                }
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .doOnComplete {
                    if (loaded && loadedIndex) {
                        showPercentage()
                    }
                }
                .subscribe({}, { it.printStackTrace() }).toString()
        }


    }


    fun loadLatest() {


        if (loading) {
            return
        }

        view.showProgress()

        loading = true

        val from = mRealm.getLastDate().toDate().time + DAY_IN_MILLIS
        val to = Calendar.getInstance().timeInMillis

        val ratingList = firebaseRating//rating.parseRating().toList()

        val dates = getDatesBetween(Date(from), Date(to))

        if (dates.isEmpty()) {
            showPercentage()
            view.hideProgress()
            loaded = true
            return
        }

        loaded = false

        val sub = Observable.fromIterable(dates)
            .flatMap { date ->
                val dateString = date.dateToString("dd-MM-yyyy")

                Observable.fromIterable(ratingList)
                    .take(100)
                    .flatMap { geckoApi.getCoinHistory(it, dateString) }
                    .map {
                        Log.d("hellokkk", it.id)
                        it.market_data.current_price.usd
                    }
                    .reduce { total: Float, next: Float ->
                        total + next
                    }
                    .toObservable()
                    .map { it / 100 }
                    .map { Pair(date, it) }
                    .retry(5)
            }
            .doOnNext { Log.d("hellokj", "date: ${it.first.dateToString()}, index: ${it.second}") }
            .doOnNext {
                val date = it.first.time.dateToString("d/M/yyyy")
                val index = it.second.toDouble().round(2)

                val realmIndex = RealmIndex()
                realmIndex.date = date
                realmIndex.index = index
                Realm.getDefaultInstance().saveRealmObject(realmIndex)
            }
            .toList()
            .toObservable()
            .doOnNext { printMissingIndexes(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnComplete {
                loading = false
                loaded = true
                refreshChart()
                if (loaded && loadedIndex) {
                    showPercentage()
                }
                view.hideProgress()
            }
            .subscribe({

            }, {
                showPercentage()
                view.hideProgress()
                it.printStackTrace()
            })
    }

    override fun refreshChart() {
        if (!loading) {
            view.refresh()
            makeChart()
        }

    }


    fun fetchFirebase() {


        firebaseRating = mFirebaseRemoteConfig.getRating()

        val missing = mRealm.findMissingDates()
        val list = ArrayList<Pair<String, Double>>()


        mRealm.executeTransaction {

            for (miss in missing) {
                val week = mFirebaseRemoteConfig.getString(miss)
                if (week != null) {
                    for (index in week.parseFirebaseWeek()) {
                        val realmIndex = RealmIndex()
                        realmIndex.date = index.first
                        realmIndex.index = index.second

                        it.insertOrUpdate(realmIndex)
                    }
                }
            }
        }
    }


    fun showPercentage() {
        view.showPercentage()
    }


    fun getDatesBetween(startDate: Date, endDate: Date): List<Date> {
        val datesInRange = ArrayList<Date>()
        val calendar = GregorianCalendar()
        calendar.time = startDate

        val endCalendar = GregorianCalendar()
        endCalendar.time = endDate

        while (calendar.before(endCalendar)) {
            val result = calendar.time
            datesInRange.add(result)
            calendar.add(Calendar.DATE, 1)
        }
        return datesInRange
    }


    fun printMissingIndexes(indexes: List<Pair<Date, Float>>): Disposable {

        Log.d("printweekdow size:", "${indexes.size}")

        return Observable.fromIterable(indexes)
            .map {
                val date = it.first.dateToString()
                val index = it.second.toDouble().round(2)

                val str = "$date=$index"

                str
            }
            .reduce { total, next ->
                "$total,$next"
            }
            .toObservable()
            .doOnNext {
                Log.d(
                    "printweekdow week:",
                    Calendar.getInstance().timeInMillis.dateToString(WEEK_DATE_PATTERN)
                )
            }
            .doOnNext { Log.d("printweekdow indexes:", it) }
            .subscribe()
    }
}

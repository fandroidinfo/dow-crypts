package info.fandroid.dowcryptsdcr.mvp.contract

import info.fandroid.dowcryptsdcr.adapter.CurrenciesAdapter
import info.fandroid.dowcryptsdcr.mvp.presenter.WideChartPresenter

class LatestChartContract {
    interface View : BaseContract.View {
        fun addEntryToChart(value: Float, date: String = "")
        fun showProgress()
        fun hideProgress()
        fun showPercentage()
        fun showErrorMessage(error: String?)
        fun refresh()
    }

    abstract class Presenter: BaseContract.Presenter<View>() {
        abstract fun makeChart(scaleEnabled: Boolean = false, distinct: WideChartPresenter.Distinct = WideChartPresenter.Distinct.Week)
        abstract fun refreshChart()
    }
}
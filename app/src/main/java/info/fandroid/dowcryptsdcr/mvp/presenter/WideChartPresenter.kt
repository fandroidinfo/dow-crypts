package info.fandroid.dowcryptsdcr.mvp.presenter

import android.util.Log
import info.fandroid.dowcryptsdcr.chart.WideChart
import info.fandroid.dowcryptsdcr.di.App
import info.fandroid.dowcryptsdcr.formatters.YearValueFormatter
import info.fandroid.dowcryptsdcr.model.RealmIndex
import info.fandroid.dowcryptsdcr.mvp.contract.LatestChartContract
import info.fandroid.ext.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import java.time.DayOfWeek
import java.util.*
import javax.inject.Inject

class WideChartPresenter : LatestChartContract.Presenter() {

    @Inject
    lateinit var formatter: YearValueFormatter

    @Inject
    lateinit var wideChart: WideChart

    init {
        App.appComponent.inject(this)
    }


    override fun makeChart(scaleEnabled: Boolean, distinct: Distinct) {

        view.showProgress()

        subscribe(
            //Observable.fromIterable(array)
            getIndexes()
                .mapWithIndex()
                .distinct {
                    getDistinctPattern(distinct, it.value().first!!)
                }
                .filter {
                    Log.d("hellol", "3: ${Thread.currentThread()}")
                    getFilterPattern(distinct, it.index(), it.value().first!!) }
                .toList()
                .toObservable()
                .doOnNext {
                    Log.d("hellol", "4: ${Thread.currentThread()}")
                    wideChart.setScaleEnabled(scaleEnabled)
                    formatter.count = it!!.size
                }
                .flatMap {
                    Log.d("hellol", "5: ${Thread.currentThread()}")
                    Observable.fromIterable(it) }
                .doOnNext {
                    Log.d("hellol", "7: ${Thread.currentThread()}")
                    view.addEntryToChart(it.value().second?.toFloat()!!, it.index().toString())
                }
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete {
                    Log.d("hellol", "6: ${Thread.currentThread()}")
                    wideChart.notifyChart()
                    wideChart.refresh()
                    view.hideProgress()
                }
                .subscribe({

                },
                    { it.printStackTrace() })
        )

//
//        subscribe(geckoApi.getCoins()
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .flatMap { Observable.fromIterable(it) }
//            .take(100)
//            .doOnNext {
//                //text.text = "${text.text}\n${it.id}: ${it.market_data?.sparkline_7d?.price.toString()}"
//            }
//            .map { it.market_data.sparkline_7d.price }
//            .sumArray()
//            .toObservable()
//            .doOnNext { formatter.count = it!!.size }
//            .flatMap { Observable.fromIterable(it) }
//            .map { it / 100 }
//            .doOnComplete {
//                //addEntry(requireActivity().findViewById<TextView>(R.id.tvCurrencySym).text.toString().toFloat())
//                //chart.highlightValue()
//            }
//            .subscribe({
//                view.addEntryToChart(it)
//
//                //text.text = "${text.text}\nsum: ${it}"
//                //text.text = "${dowList.size}\n${dowList.toString()}"
//            }, { it.printStackTrace() })
//        )

    }

    enum class Distinct(val value: Long) {
        Day(1L),
        Week(7L),
        Month(1L)
    }

    private fun getDistinctPattern(distinct: Distinct, date: String): String {
        val calendar = date.toCalendar()

        return when (distinct) {
            Distinct.Day -> calendar.dayMonthYear()
            Distinct.Week -> calendar.dayMonthYear()
            Distinct.Month -> calendar.monthYear()
        }
    }


    private fun getFilterPattern(distinct: Distinct, index: Long, date: String): Boolean {
        if (distinct == Distinct.Week) {
            return date.toCalendar().get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY
        }
        return index % distinct.value == 0L
    }


    override fun refreshChart() {

    }

    fun getIndexes(): Observable<Pair<String?, Double?>> {
        return Observable.just("a")
            .flatMap {
                Log.d("hellol", "2: ${Thread.currentThread()}")
                Observable.fromIterable(Realm.getDefaultInstance().where(RealmIndex::class.java).findAll())
            }
            .map { Pair(it.date, it.index) }
    }
}
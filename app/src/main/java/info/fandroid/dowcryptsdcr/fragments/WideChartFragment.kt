package info.fandroid.dowcryptsdcr.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import info.fandroid.dowcryptsdcr.R
import info.fandroid.dowcryptsdcr.chart.WideChart
import info.fandroid.dowcryptsdcr.di.App
import info.fandroid.dowcryptsdcr.mvp.contract.LatestChartContract
import info.fandroid.dowcryptsdcr.mvp.presenter.WideChartPresenter
import info.fandroid.ext.WEEK_IN_MILLIS
import kotlinx.android.synthetic.main.fragment_wide_chart.*
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 *
 */
class WideChartFragment : Fragment() , LatestChartContract.View{

    @Inject
    lateinit var latestChart: WideChart

    @Inject
    lateinit var presenter: WideChartPresenter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_wide_chart, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        App.appComponent.inject(this)

        presenter.attach(this)


        latestChart.initChart(chart)


        presenter.makeChart()

        btnWeek.isChecked = true


        btnDay.setOnTouchListener { v, event ->
            btnDay.isChecked
        }

        btnDay.setOnCheckedChangeListener { buttonView, isChecked ->
            if (btnDay.isChecked) {
                //latestChart.mScale = 8f

                btnMonth.isChecked = false
                btnWeek.isChecked = false

                clearChart()
                presenter.makeChart(false, WideChartPresenter.Distinct.Day)
            }
        }


        btnWeek.setOnTouchListener { v, event ->
            btnWeek.isChecked
        }

        btnWeek.setOnCheckedChangeListener { buttonView, isChecked ->
            if (btnWeek.isChecked) {
                //latestChart.mScale = 3f

                btnDay.isChecked = false
                btnMonth.isChecked = false

                clearChart()
                presenter.makeChart(true, WideChartPresenter.Distinct.Week)
            }
        }


        btnMonth.setOnTouchListener { v, event ->
            btnMonth.isChecked
        }

        btnMonth.setOnCheckedChangeListener { buttonView, isChecked ->
            if (btnMonth.isChecked) {
                //latestChart.mScale = 1f

                btnDay.isChecked = false
                btnWeek.isChecked = false

                clearChart()
                presenter.makeChart(true, WideChartPresenter.Distinct.Month)
            }
        }

        btnWideOut.setOnClickListener {
            requireActivity().finish()
        }

    }




    private fun clearChart() {
        chart.highlightValue(null)
        chart.data.clearValues()
    }


    override fun refresh() {

    }



    override fun addEntryToChart(value: Float, date: String) {


        latestChart.addEntry(value, date)

    }

    override fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress.visibility = View.INVISIBLE
    }

    override fun showErrorMessage(error: String?) {

    }

    override fun showPercentage() {

    }
}

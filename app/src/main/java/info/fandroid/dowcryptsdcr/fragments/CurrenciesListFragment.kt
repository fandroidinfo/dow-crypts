package info.fandroid.dowcryptsdcr.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import info.fandroid.dowcryptsdcr.di.App
import info.fandroid.dowcryptsdcr.R
import info.fandroid.dowcryptsdcr.adapter.BaseAdapter
import info.fandroid.dowcryptsdcr.adapter.CurrenciesAdapter
import info.fandroid.dowcryptsdcr.mvp.contract.CurrenciesContract
import info.fandroid.dowcryptsdcr.mvp.presenter.CurrenciesPresenter
import info.fandroid.dowcryptsdcr.rest.CoinCapApi
import info.fandroid.dowcryptsdcr.rest.CoinGeckoApi
import info.fandroid.ext.round
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class CurrenciesListFragment : BaseListFragment(), CurrenciesContract.View {


    @Inject
    lateinit var presenter: CurrenciesPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_item_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        App.appComponent.inject(this)
        presenter.attach(this)

        presenter.makeIndex()
    }


    override fun showIndex(index: String) {
        requireActivity().findViewById<TextView>(R.id.tvCurrencySym)?.text = index
    }

    override fun showIndexChanges(percent: Double) {
        val percentView = requireActivity().tvPercent
        percentView.text = String.format("%.2f", Math.abs(percent.round(2))) + "%"

        if (percent > 0) {
            requireActivity().arrow.setImageResource(R.drawable.arrowup)
            percentView.setTextColor(resources.getColor(R.color.greenPercent))
        } else {
            requireActivity().arrow.setImageResource(R.drawable.arrowdown)
            percentView.setTextColor(resources.getColor(R.color.redPercent))
        }

        requireActivity().percentContainer.visibility = View.VISIBLE
    }

    override fun addCurrency(currency: CurrenciesAdapter.Currency) {
        viewAdapter.add(currency)
    }

    override fun notifyAdapter() {
        viewAdapter.notifyDataSetChanged()
    }

    override fun showProgress() {
        requireActivity().progress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        requireActivity().progress.visibility = View.INVISIBLE
    }

    override fun showErrorMessage(error: String?) {
        Toast.makeText(context, "No internet connection.", Toast.LENGTH_SHORT).show()
    }


    override fun createAdapterInstance(): BaseAdapter<*> {
        return CurrenciesAdapter()
    }

    override fun refresh() {
        viewAdapter.items.clear()
        viewAdapter.notifyDataSetChanged()
    }
}

package info.fandroid.dowcryptsdcr.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.remoteconfig.FirebaseRemoteConfig

import info.fandroid.dowcryptsdcr.R
import info.fandroid.dowcryptsdcr.adapter.TabsPagerAdapter
import info.fandroid.dowcryptsdcr.di.App
import info.fandroid.dowcryptsdcr.mvp.presenter.CurrenciesPresenter
import info.fandroid.dowcryptsdcr.mvp.presenter.LatestChartPresenter
import kotlinx.android.synthetic.main.fragment_main.*
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 *
 */
class MainFragment : Fragment() {

    @Inject
    lateinit var currenciesPresenter: CurrenciesPresenter

    @Inject
    lateinit var latestChartPresenter: LatestChartPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        App.appComponent.inject(this)


        swipeRefreshLayout.setColorScheme(
            R.color.colorRed,
            R.color.colorBlue,
            R.color.colorGreen
        )
        swipeRefreshLayout.setOnRefreshListener {
            loadIndex()
        }



        val fragmentAdapter = TabsPagerAdapter(requireActivity().supportFragmentManager)
        viewpager.adapter = fragmentAdapter

        tabs.setupWithViewPager(viewpager)




        val mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance()

//        mFirebaseRemoteConfig.fetch()
//            .addOnCompleteListener(this) { task ->
//                if (task.isSuccessful) {
//                    Toast.makeText(
//                        this@MainActivity, "Fetch Succeeded",
//                        Toast.LENGTH_SHORT
//                    ).show()
//
//
//                    // After config data is successfully fetched, it must be activated before newly fetched
//                    // values are returned.
//                    mFirebaseRemoteConfig.activateFetched()
//                    //textView.text = mFirebaseRemoteConfig.getString("_3253426456456")
//                    //textView.text = mFirebaseRemoteConfig.getString("hello2")
//                    //val configCurrencies = mFirebaseRemoteConfig.getString("_hello_there")
//                    val configCurrencies = mFirebaseRemoteConfig.getString("_3253426456456")
//                    //textView.text = configCurrencies
//
//                    val configCurrenciesList = configCurrencies.split(",")
//
//                    textView.text = configCurrenciesList.size.toString()
//
////                    for (cur in 1..50) {
////                        textView.text = "${textView.text} ${configCurrenciesList[cur]}"
////                    }
//
//
//
//                } else {
//                    Toast.makeText(
//                        this@MainActivity, "Fetch Failed",
//                        Toast.LENGTH_SHORT
//                    ).show()
//
//                    task.exception?.printStackTrace()
//                }
//
//
//            }

    }



    private fun loadIndex() {
        currenciesPresenter.refreshIndex()
        latestChartPresenter.refreshChart()

        onItemsLoadComplete()
    }

    private fun onItemsLoadComplete() {

        swipeRefreshLayout.isRefreshing = false
    }




}

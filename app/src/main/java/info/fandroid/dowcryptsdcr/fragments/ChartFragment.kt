package info.fandroid.dowcryptsdcr.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import info.fandroid.dowcryptsdcr.di.App
import kotlinx.android.synthetic.main.fragment_chart.*
import javax.inject.Inject
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import info.fandroid.dowcryptsdcr.*
import info.fandroid.dowcryptsdcr.mvp.contract.LatestChartContract
import info.fandroid.dowcryptsdcr.mvp.presenter.LatestChartPresenter
import android.content.Intent
import android.graphics.Color
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import info.fandroid.dowcryptsdcr.activities.WideChartActivity
import info.fandroid.dowcryptsdcr.chart.LatestChart
import info.fandroid.dowcryptsdcr.model.RealmIndex
import info.fandroid.ext.round
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_main.*
import org.w3c.dom.Text


class ChartFragment : Fragment(), OnChartValueSelectedListener, LatestChartContract.View {


    @Inject
    lateinit var latestChart: LatestChart

    @Inject
    lateinit var presenter: LatestChartPresenter

    var latestChartValue: Float = 0F

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_chart, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        App.appComponent.inject(this)

        presenter.attach(this)

//        val raw =
//            " ,bitcoin,ethereum,ripple,bitcoin-cash,stellar,eos,litecoin,cardano,tether,monero,tron,iota,dash,binance-coin,neo,ethereum-classic,nem,tezos,zcash,vechain,bitcoin-gold,maker,omisego,0x,dogecoin,decred,qtum,ontology,lisk,basic-attention-token,zilliqa,aeternity,bitshares,bitcoin-diamond,nano,bytecoin-bcn,icon,siacoin,digibyte,pundi-x,steem,verge,bytom,chainlink,populous,waves,metaverse,augur,trueusd,golem-network-tokens,stratis,komodo,holo,status,electroneum,waltonchain,maidsafecoin,wax,wanchain,ardor,mithril,iostoken,aion,decentraland,revain,ravencoin,bancor,loopring,nexo,huobi-token,digixdao,aelf,zencash,qash,hypercash,ark,gxchain,polymath-network,pivx,reddcoin,monacoin,crypto-com,loom-network,cybermiles,nxt,funfair,nebulas-token,tenx,theta-token,power-ledger,zcoin,boscoin,kyber-network,salt,gas,elastos,syscoin,enigma-project,civic,gochain"
//        val currencies = ArrayList(raw.split(","))
//        currencies.removeAll(Arrays.asList("", " ", null))
//
//
//        val id = "bitcoin"
//        val interval = "d1"
//        val start: Long = 1540771200000
//        val end: Long = 1541289600000
//        val formatter = HourValueFormatter(0)
//


        latestChart.initChart(chart)

        //presenter.makeChart()

        btnWide.setOnClickListener {
            val intent = Intent(activity, WideChartActivity::class.java)
            startActivity(intent)
        }


    }


    override fun addEntryToChart(value: Float, date: String) {


        latestChartValue = value

        latestChart.addEntry(value, date)


    }

    override fun showProgress() {
        progressBottom.visibility = View.VISIBLE

    }

    override fun hideProgress() {
        progressBottom.visibility = View.INVISIBLE
        requireActivity().progressPercent.visibility = View.INVISIBLE
    }

    override fun showErrorMessage(error: String?) {

    }


    override fun refresh() {
        chart.highlightValue(null)
        chart.data.clearValues()
    }

    override fun showPercentage() {
        val textView = requireActivity().tvCurrencySym

        val query = Realm.getDefaultInstance().where(RealmIndex::class.java).findAll()
        latestChartValue = query.get(query.size - 1)?.index?.toFloat()!!

//        while (!textView.text.isEmpty()) {
//            showPercentage()
//            return
//        }

        if (!textView.text.isEmpty()) {

            val latestIndex = textView.text.toString().toDouble()

            val percent = ((latestIndex - latestChartValue) / latestIndex) * 100
            val percentView = requireActivity().tvPercent
            percentView.text = String.format("%.2f", Math.abs(percent.round(2))) + "%"

            if (latestChartValue.toDouble().round(3) < latestIndex) {
                requireActivity().arrow.setImageResource(R.drawable.arrowup)
                percentView.setTextColor(resources.getColor(R.color.greenPercent))
            } else {
                requireActivity().arrow.setImageResource(R.drawable.arrowdown)
                percentView.setTextColor(resources.getColor(R.color.redPercent))
            }

            if (progressPercent != null) {
                progressPercent.visibility = View.INVISIBLE
            }
            requireActivity().percentContainer.visibility = View.VISIBLE
        }

        hideProgress()


    }

    override fun onNothingSelected() {

    }

    override fun onValueSelected(e: Entry?, h: Highlight?) {

    }

}

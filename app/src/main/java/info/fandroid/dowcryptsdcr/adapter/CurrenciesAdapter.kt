package info.fandroid.dowcryptsdcr.adapter

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import info.fandroid.dowcryptsdcr.R
import info.fandroid.dowcryptsdcr.model.GeckoCoin
import info.fandroid.dowcryptsdcr.mvp.presenter.CurrenciesPresenter
import info.fandroid.ext.formatThousands
import info.fandroid.ext.round
import kotlinx.android.synthetic.main.recycler_view_item.view.*
import javax.inject.Inject

class CurrenciesAdapter : BaseAdapter<CurrenciesAdapter.CurrencyViewHolder>() {


    val imageMap = HashMap<String, String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.recycler_view_item, parent, false)
        return CurrencyViewHolder(v)
    }

    class CurrencyViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {


        fun bindPhoto(image: String?) {

        }


        override fun bind(item: Any) {
            let {
                item as Currency
                Glide.with(view.context).load(item.image).into(view.ivCurrencyIcon)
                view.tvCurrencySym.text = item.symbol
                view.tvCurrencyName.text = item.name
                view.tvCurrencyMarketCap.text = "Cap: ${item.marketCap}"
                view.tvCurrencyPrice.text = "Price: $${item.price}"
            }
        }
    }

    data class Currency(
        val id: String,
        val symbol: String,
        val name: String,
        val marketCap: String,
        val price: Double,
        val image: String
    ) {
        constructor(coin: GeckoCoin) : this(
            coin.id,
            coin.symbol,
            coin.name,
            coin.market_cap.formatThousands(),
            coin.current_price.round(3),
            coin.image
        )
    }
}
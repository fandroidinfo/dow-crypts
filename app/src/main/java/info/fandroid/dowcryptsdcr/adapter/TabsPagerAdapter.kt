package info.fandroid.dowcryptsdcr.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import info.fandroid.dowcryptsdcr.fragments.ChartFragment
import info.fandroid.dowcryptsdcr.fragments.CurrenciesListFragment

class TabsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                CurrenciesListFragment()
            }

            else -> {
                return ChartFragment()
            }
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "top-100 currencies"
            else -> {
                return "Index Chart"
            }
        }
    }

}
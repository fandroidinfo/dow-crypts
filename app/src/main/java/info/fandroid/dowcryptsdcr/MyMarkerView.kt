package info.fandroid.dowcryptsdcr

import com.github.mikephil.charting.utils.MPPointF
import android.widget.TextView
import com.github.mikephil.charting.components.MarkerView
import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import info.fandroid.dowcryptsdcr.model.RealmIndex
import info.fandroid.ext.dateToString
import info.fandroid.ext.toDate
import io.realm.Realm
import java.util.*


@SuppressLint("ViewConstructor")
class MyMarkerView(context: Context, layoutResource: Int) : MarkerView(context, layoutResource) {

    private val tvContent: TextView = findViewById(R.id.tvContent)

    var isScaleEnabled = false

    private var query: MutableList<RealmIndex> = ArrayList()

    init {
        query.addAll(Realm.getDefaultInstance().where(RealmIndex::class.java).findAll())
    }

    // runs every time the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    override fun refreshContent(e: Entry, highlight: Highlight) {
        val date = query[e.x.toInt()].date?.toDate()


        tvContent.text = e.y.toString() + "\n" + date?.dateToString("MMM d, yyyy")

        super.refreshContent(e, highlight)
    }

    override fun getOffset(): MPPointF {
        val metrics = Resources.getSystem().getDisplayMetrics()
        return MPPointF((-(width / 2)).toFloat(), (-metrics.heightPixels).toFloat())
    }

}
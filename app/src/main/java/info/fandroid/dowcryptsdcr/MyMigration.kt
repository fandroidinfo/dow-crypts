package info.fandroid.dowcryptsdcr

import info.fandroid.dowcryptsdcr.model.RealmIndex
import io.realm.DynamicRealm
import io.realm.RealmMigration
import io.realm.RealmObjectSchema
import io.realm.RealmSchema



class MyMigration : RealmMigration {
    override fun migrate(realm: DynamicRealm, oldVersion: Long, newVersion: Long) {
        val schema = realm.schema
        val aSchema = schema.get("RealmIndex")
        //aSchema!!.setRequired("date", true)
        aSchema!!.addIndex("date")
        aSchema!!.addPrimaryKey("date") // also internally adds a search index!!!
    }
}
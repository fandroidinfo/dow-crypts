package info.fandroid.dowcryptsdcr.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import info.fandroid.dowcryptsdcr.R
import info.fandroid.dowcryptsdcr.adapter.TabsPagerAdapter
import info.fandroid.dowcryptsdcr.di.App
import info.fandroid.dowcryptsdcr.mvp.presenter.CurrenciesPresenter
import info.fandroid.dowcryptsdcr.mvp.presenter.LatestChartPresenter
import info.fandroid.ext.findMissingDates
import info.fandroid.ext.getRating
import info.fandroid.ext.parseFirebaseWeek
import info.fandroid.ext.saveRealmObject
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var currenciesPresenter: CurrenciesPresenter

    @Inject
    lateinit var latestChartPresenter: LatestChartPresenter

    @Inject
    lateinit var mFirebaseRemoteConfig: FirebaseRemoteConfig

    val timeout = 3600L


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        progress.visibility = View.VISIBLE

        App.appComponent.inject(this)


        fetchFirebase()


        swipeRefreshLayout.setColorScheme(
            R.color.colorRed,
            R.color.colorBlue,
            R.color.colorGreen
        )
        swipeRefreshLayout.setOnRefreshListener {
            loadIndex()
        }



        val fragmentAdapter = TabsPagerAdapter(supportFragmentManager)
        viewpager.adapter = fragmentAdapter

        tabs.setupWithViewPager(viewpager)




        val mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance()

//        mFirebaseRemoteConfig.fetch()
//            .addOnCompleteListener(this) { task ->
//                if (task.isSuccessful) {
//                    Toast.makeText(
//                        this@MainActivity, "Fetch Succeeded",
//                        Toast.LENGTH_SHORT
//                    ).show()
//
//
//                    // After config data is successfully fetched, it must be activated before newly fetched
//                    // values are returned.
//                    mFirebaseRemoteConfig.activateFetched()
//                    //textView.text = mFirebaseRemoteConfig.getString("_3253426456456")
//                    //textView.text = mFirebaseRemoteConfig.getString("hello2")
//                    //val configCurrencies = mFirebaseRemoteConfig.getString("_hello_there")
//                    val configCurrencies = mFirebaseRemoteConfig.getString("_3253426456456")
//                    //textView.text = configCurrencies
//
//                    val configCurrenciesList = configCurrencies.split(",")
//
//
//
////                    for (cur in 1..50) {
////                        textView.text = "${textView.text} ${configCurrenciesList[cur]}"
////                    }
//
//
//
//                } else {
//                    Toast.makeText(
//                        this@MainActivity, "Fetch Failed",
//                        Toast.LENGTH_SHORT
//                    ).show()
//
//                    task.exception?.printStackTrace()
//                }
//
//
//            }


    }


    private fun loadIndex() {
        currenciesPresenter.refreshIndex()
        latestChartPresenter.refreshChart()

        mFirebaseRemoteConfig.fetch(timeout)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {

                    mFirebaseRemoteConfig.activateFetched()

                    latestChartPresenter.fetchFirebase()
                    latestChartPresenter.refreshChart()

                    if (latestChartPresenter.loadedIndex && latestChartPresenter.loaded) {
                        latestChartPresenter.showPercentage()
                    }

                    latestChartPresenter.fetched = true
                } else {
                    task.exception?.printStackTrace()
                }
            }


        onItemsLoadComplete()
    }


    private fun fetchFirebase() {
        mFirebaseRemoteConfig.fetch(timeout)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {

                    mFirebaseRemoteConfig.activateFetched()

                    latestChartPresenter.fetchFirebase()
                    latestChartPresenter.makeChart()

                    if (latestChartPresenter.loadedIndex && latestChartPresenter.loaded) {
                        latestChartPresenter.showPercentage()
                    }

                    latestChartPresenter.fetched = true
                } else {
                    task.exception?.printStackTrace()
                }
            }
    }

    private fun onItemsLoadComplete() {

        swipeRefreshLayout.isRefreshing = false
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item!!.itemId) {
            R.id.action_info -> {

                intent = Intent(this, InfoTextActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.action_privacy -> {

                intent = Intent(Intent.ACTION_VIEW)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.data = Uri.parse("http://privacypolicy.fandroid.info/DowCrypts_Privacy_Policy.pdf")
                startActivity(intent)
                return true
            }

        }
        return super.onOptionsItemSelected(item)
    }





}


package info.fandroid.dowcryptsdcr.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import info.fandroid.dowcryptsdcr.R

class InfoTextActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info_text)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Info"
    }
}

package info.fandroid.dowcryptsdcr.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import info.fandroid.dowcryptsdcr.di.App
import info.fandroid.dowcryptsdcr.mvp.contract.LatestChartContract
import info.fandroid.dowcryptsdcr.mvp.presenter.WideChartPresenter
import javax.inject.Inject
import info.fandroid.dowcryptsdcr.R
import info.fandroid.dowcryptsdcr.chart.WideChart
import info.fandroid.dowcryptsdcr.fragments.WideChartFragment
import kotlinx.android.synthetic.main.activity_wide_chart.*


class WideChartActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wide_chart)


        supportFragmentManager.beginTransaction().replace(R.id.container, WideChartFragment()).commit()
    }



}

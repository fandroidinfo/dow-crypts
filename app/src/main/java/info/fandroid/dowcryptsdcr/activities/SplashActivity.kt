package info.fandroid.dowcryptsdcr.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_splash.*
import android.content.SharedPreferences
import android.content.Context
import info.fandroid.dowcryptsdcr.R


class SplashActivity : AppCompatActivity() {

    var wasOpenedInfo : Boolean = false

    lateinit var loginPreference: SharedPreferences
    var MY_PREF = "my_pref"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        buttonGo.setOnClickListener {
            vokeMainActivity()
        }


        loginPreference = getSharedPreferences(MY_PREF, Context.MODE_PRIVATE)

        // this condition will do the trick.

        if (loginPreference.getString("tag", "notok").equals("notok")) {

            // add tag in SharedPreference here..
            val edit = loginPreference.edit()
            edit.putString("tag", "ok")
            edit.commit()



        } else if (loginPreference.getString("tag", null).equals("ok")) {
            vokeMainActivity()
        }

        wasOpenedInfo = intent.getBooleanExtra(getString(R.string.WasOpenedInfo), false)
    }

    private fun vokeMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    override fun onBackPressed() {
        if (wasOpenedInfo == true) vokeMainActivity()
        super.onBackPressed()
    }
}

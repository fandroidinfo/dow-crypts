package info.fandroid.dowcryptsdcr

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.widget.TextView
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF
import java.util.*
import info.fandroid.dowcryptsdcr.di.App
import info.fandroid.dowcryptsdcr.formatters.HourValueFormatter
import info.fandroid.dowcryptsdcr.model.RealmIndex
import info.fandroid.ext.dateToString
import info.fandroid.ext.round
import info.fandroid.ext.toDate
import io.realm.Realm
import javax.inject.Inject


@SuppressLint("ViewConstructor")
class HourMarkerView(context: Context, layoutResource: Int) : MarkerView(context, layoutResource) {

    private val tvContent: TextView = findViewById(R.id.tvContent)

    @Inject
    lateinit var formatter: HourValueFormatter


    private var query: MutableList<RealmIndex> = ArrayList()


    init {
        App.appComponent.inject(this)
    }

    fun initList() {
        query.clear()
        query.addAll(Realm.getDefaultInstance().where(RealmIndex::class.java).findAll())
    }

    // runs every time the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    override fun refreshContent(e: Entry, highlight: Highlight) {

        val date = query[e.x.toInt()].date?.toDate()


        tvContent.text = e.y.toString() + "\n" + date?.dateToString("MMM d")

        super.refreshContent(e, highlight)
    }

    override fun getOffset(): MPPointF {
        val metrics = Resources.getSystem().getDisplayMetrics()
        return MPPointF((-(width / 2)).toFloat(), (-metrics.heightPixels).toFloat())
    }
}
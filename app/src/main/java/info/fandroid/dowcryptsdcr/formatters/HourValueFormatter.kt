package info.fandroid.dowcryptsdcr.formatters

import android.util.Log
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.DefaultValueFormatter;
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.formatter.IValueFormatter
import com.github.mikephil.charting.utils.ViewPortHandler
import info.fandroid.dowcryptsdcr.model.RealmIndex
import info.fandroid.ext.dateToString
import info.fandroid.ext.toDate
import io.realm.Realm
import java.util.*
import kotlin.collections.ArrayList

class HourValueFormatter(var count: Int) : IAxisValueFormatter {


    private val mMonths = arrayOf("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")

    private var query: MutableList<RealmIndex> = ArrayList()



    fun initList() {
        query.clear()
        query.addAll(Realm.getDefaultInstance().where(RealmIndex::class.java).findAll())
    }



    override fun getFormattedValue(value: Float, axis: AxisBase?): String {
        val date = query[value.toInt()].date?.toDate()

        val calendar = Calendar.getInstance()
        calendar.timeInMillis = date?.time!!


        return calendar.toFormatted()
    }

    fun Calendar.toFormatted(): String {
        return "${get(Calendar.DAY_OF_MONTH)} ${mMonths[get(Calendar.MONTH)]}"// \n${get(Calendar.HOUR_OF_DAY)}:${get(Calendar.MINUTE)}"
    }

}
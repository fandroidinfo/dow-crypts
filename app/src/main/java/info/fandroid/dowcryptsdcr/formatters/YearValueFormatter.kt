package info.fandroid.dowcryptsdcr.formatters

import android.util.Log
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import info.fandroid.dowcryptsdcr.model.RealmIndex
import info.fandroid.ext.dateToString
import info.fandroid.ext.toDate
import io.realm.Realm
import java.util.*

class YearValueFormatter (var count: Int) : IAxisValueFormatter {


    private val mMonths = arrayOf("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")

    var isScaleEnabled = false


    private var query: MutableList<RealmIndex> = ArrayList()

    init {
        query.addAll(Realm.getDefaultInstance().where(RealmIndex::class.java).findAll())
    }

    override fun getFormattedValue(value: Float, axis: AxisBase?): String {
        val date = query[value.toInt()].date?.toDate()

        val calendar = Calendar.getInstance()
        calendar.timeInMillis = date?.time!!//calendar.timeInMillis - ((count - value.toLong()) * 3600000 * 24)//(value.toLong() * 100 * 60 * 60)

        Log.d("hhhd", "date: ${calendar.timeInMillis.dateToString()}, size: $count")

        return calendar.toFormatted()
    }

    fun Calendar.toFormatted(): String {
        return "${mMonths[get(Calendar.MONTH)]} ${get(Calendar.YEAR)}"// \n${get(Calendar.HOUR_OF_DAY)}:${get(Calendar.MINUTE)}"
    }

}
package info.fandroid.dowcryptsdcr.model

data class AssetsHistoryResponse(
    val data: List<CurrencyHistory>,
    val timestamp: Float
)

data class CurrencyHistory (
    val priceUsd: Float,
    val time: Float
)
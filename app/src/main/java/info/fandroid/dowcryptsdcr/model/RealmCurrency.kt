// Please note : @LinkingObjects and default values are not represented in the schema and thus will not be part of the generated models
package info.fandroid.dowcryptsdcr.model

import io.realm.RealmObject

open class RealmCurrency {
    var symbol: String? = null
    var name: String? = null
    var date: String? = null
    var ranknow: Long? = null
    var open: Double? = null
    var close: Double? = null
    var market: Long? = null
}

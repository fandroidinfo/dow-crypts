package info.fandroid.dowcryptsdcr.model

data class GeckoCoin(
    val id: String,
    val symbol: String,
    val name: String,
    //val image: GeckoCoinImage,
    val image: String,
    val market_cap: Double,
    val current_price: Double,
    val sparkline_in_7d: Sparkline,
    val market_data: MarketData,
    val price_change_24h: Double
)

data class GeckoCoinImage(val small: String,
                          val large: String)

data class MarketData(val sparkline_in_7d: Sparkline,
                      val market_cap: Usd,
                      val current_price: Usd)

data class Sparkline(val price: List<Float>)

data class Usd(val usd: Float)
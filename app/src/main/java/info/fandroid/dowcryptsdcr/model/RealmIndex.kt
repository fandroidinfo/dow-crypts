package info.fandroid.dowcryptsdcr.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class RealmIndex : RealmObject() {
    @PrimaryKey
    var date: String? = null

    var index: Double? = null
}
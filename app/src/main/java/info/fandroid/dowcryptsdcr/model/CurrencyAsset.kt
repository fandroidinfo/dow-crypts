package info.fandroid.dowcryptsdcr.model


data class AssetsResult(
    val data: List<CurrencyAsset>,
    val timestamp: Float
)

data class CurrencyAsset (
    val id: String,
    val rank: Int,
    val symbol: String,
    val name: String,
    val supply: Float,
    val maxSupply: Float,
    val marketCapUsd: Float,
    val volumeUsd24Hr: Float,
    val priceUsd: Float,
    val changePercent24Hr: Float,
    val vwap24Hr: Float
)
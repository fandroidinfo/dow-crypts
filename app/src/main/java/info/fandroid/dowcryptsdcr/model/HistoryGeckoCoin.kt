package info.fandroid.dowcryptsdcr.model

data class HistoryGeckoCoin(
    val id: String,
    val symbol: String,
    val name: String,
    val market_data: MarketData
)

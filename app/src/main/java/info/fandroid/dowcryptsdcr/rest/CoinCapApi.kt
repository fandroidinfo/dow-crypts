package info.fandroid.dowcryptsdcr.rest

import info.fandroid.dowcryptsdcr.model.AssetsHistoryResponse
import info.fandroid.dowcryptsdcr.model.AssetsResult
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface CoinCapApi {

    @GET("assets")
    fun getAssets(@Query("limit") limit: Int): Observable<AssetsResult>


    @GET("assets/{id}/history")
    fun getAssetsHistory(
        @Path("id") id: String,
        @Query("start") start: Long,
        @Query("end") end: Long,
        @Query("interval") interval: String = "h1"
    ): Observable<AssetsHistoryResponse>


}
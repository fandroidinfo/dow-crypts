package info.fandroid.dowcryptsdcr.rest

import info.fandroid.dowcryptsdcr.model.AssetsHistoryResponse
import info.fandroid.dowcryptsdcr.model.AssetsResult
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface NomixApi {

    @GET("currencies/sparkline")
    fun getCurrenciesSparkline(@Query("key") key: String): Observable<AssetsResult>


    @GET("assets/{id}/history")
    fun getAssetsHistory(@Path("id") id: String,
                         @Query("interval") interval: String,
                         @Query("start") start: Long,
                         @Query("end") end: Long): Observable<AssetsHistoryResponse>
}
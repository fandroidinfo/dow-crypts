package info.fandroid.dowcryptsdcr.rest

import info.fandroid.dowcryptsdcr.model.AssetsHistoryResponse
import info.fandroid.dowcryptsdcr.model.AssetsResult
import info.fandroid.dowcryptsdcr.model.GeckoCoin
import info.fandroid.dowcryptsdcr.model.HistoryGeckoCoin
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface CoinGeckoApi {

    @GET("coins")
    fun getCoins(
        @Query("per_page") perPage: Int = 100,
        @Query("sparkline") sparkline: Boolean = true,
        @Query("order") order: String = "market_cap_desc"
    ): Observable<List<GeckoCoin>>


    @GET("coins/markets")
    fun getMarketCoins(
        @Query("vs_currency") vs: String = "usd",
        @Query("per_page") perPage: Int = 100,
        @Query("sparkline") sparkline: Boolean = true,
        @Query("order") order: String = "market_cap_desc"
    ): Observable<List<GeckoCoin>>


    @GET("coins/list")
    fun getCoinsList(
    ): Observable<List<GeckoCoin>>


    @GET("coins/{id}")
    fun getCoin(
        @Path("id") id: String,
        @Query("tickers") tickers: Boolean = false,
        @Query("market_data") marketData: Boolean = false,
        @Query("community_data") communityData: Boolean = false,
        @Query("developer_data") developerData: Boolean = false,
        @Query("sparkline") sparkline: Boolean = false
    ): Observable<GeckoCoin>

    @GET("coins/{id}/history")
    fun getCoinHistory(
        @Path("id") id: String,
        @Query("date") date: String,
        @Query("localization") localization: String = "false"
    ): Observable<HistoryGeckoCoin>

}
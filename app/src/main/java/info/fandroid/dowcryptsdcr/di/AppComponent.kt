package info.fandroid.dowcryptsdcr.di

import dagger.Component
import info.fandroid.dowcryptsdcr.HourMarkerView
import info.fandroid.dowcryptsdcr.chart.LatestChart
import info.fandroid.dowcryptsdcr.activities.MainActivity
import info.fandroid.dowcryptsdcr.chart.WideChart
import info.fandroid.dowcryptsdcr.activities.WideChartActivity
import info.fandroid.dowcryptsdcr.fragments.ChartFragment
import info.fandroid.dowcryptsdcr.fragments.CurrenciesListFragment
import info.fandroid.dowcryptsdcr.fragments.MainFragment
import info.fandroid.dowcryptsdcr.fragments.WideChartFragment
import info.fandroid.dowcryptsdcr.mvp.presenter.CurrenciesPresenter
import info.fandroid.dowcryptsdcr.mvp.presenter.LatestChartPresenter
import info.fandroid.dowcryptsdcr.mvp.presenter.WideChartPresenter
import javax.inject.Singleton

@Component(modules = arrayOf(AppModule::class, RestModule::class, MvpModule::class, ChartModule::class))
@Singleton
interface AppComponent {

    fun inject(mainActivity: MainActivity)
    fun inject(activity: WideChartActivity)

    fun inject(fragment: CurrenciesListFragment)
    fun inject(fragment: ChartFragment)
    fun inject(fragment: WideChartFragment)
    fun inject(fragment: MainFragment)

    fun inject(presenter: CurrenciesPresenter)
    fun inject(presenter: LatestChartPresenter)
    fun inject(presenter: WideChartPresenter)

    fun inject(chart: LatestChart)
    fun inject(chart: WideChart)

    fun inject(marker: HourMarkerView)
}
package info.fandroid.dowcryptsdcr.di

import android.app.Application
import com.google.firebase.FirebaseApp
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig
import info.fandroid.dowcryptsdcr.R
import info.fandroid.dowcryptsdcr.model.RealmIndex
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.annotations.RealmModule


class App : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }


    override fun onCreate() {
        super.onCreate()

        Realm.init(this)
        val config = RealmConfiguration.Builder()
            .assetFile("my.realm")
            .modules(MyModule())
            .schemaVersion(10)
            //.migration(MyMigration())
            .build()
        Realm.setDefaultConfiguration(config)

        FirebaseApp.initializeApp(this)

        val mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance()
        mFirebaseRemoteConfig.setDefaults(R.xml.xml_defaults)

        initializeDagger()

        // Создание расширенной конфигурации библиотеки.
        val metricaConfig = YandexMetricaConfig.newConfigBuilder("45d6cae5-c908-43eb-a4d6-3f416f57942a").build()
        // Инициализация AppMetrica SDK.
        YandexMetrica.activate(applicationContext, metricaConfig)
        // Отслеживание активности пользователей.
        YandexMetrica.enableActivityAutoTracking(this)

    }

    fun initializeDagger() {

        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .restModule(RestModule())
            .mvpModule(MvpModule())
            .chartModule(ChartModule())
            .build()
    }
}

@RealmModule(classes = [RealmIndex::class])
class MyModule
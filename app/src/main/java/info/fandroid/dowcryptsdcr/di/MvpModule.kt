package info.fandroid.dowcryptsdcr.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import info.fandroid.dowcryptsdcr.mvp.presenter.CurrenciesPresenter
import info.fandroid.dowcryptsdcr.mvp.presenter.LatestChartPresenter
import info.fandroid.dowcryptsdcr.mvp.presenter.WideChartPresenter
import javax.inject.Singleton

@Module
class MvpModule {

    @Provides
    @Singleton
    fun provideCurrenciesPresenter(): CurrenciesPresenter = CurrenciesPresenter()

    @Provides
    @Singleton
    fun provideLatestChartPresenter(): LatestChartPresenter = LatestChartPresenter()

    @Provides
    @Singleton
    fun provideWideChartPresenter(): WideChartPresenter = WideChartPresenter()
}
package info.fandroid.dowcryptsdcr.di

import dagger.Module
import dagger.Provides
import info.fandroid.dowcryptsdcr.formatters.HourValueFormatter
import info.fandroid.dowcryptsdcr.chart.LatestChart
import info.fandroid.dowcryptsdcr.chart.WideChart
import info.fandroid.dowcryptsdcr.formatters.YearValueFormatter
import javax.inject.Singleton

@Module
class ChartModule {

    @Provides
    @Singleton
    fun provideLatestChart() = LatestChart()

    @Provides
    @Singleton
    fun provideHourFormatter() = HourValueFormatter(0)


    @Provides
    @Singleton
    fun provideWideChart() = WideChart()

    @Provides
    @Singleton
    fun provideYearFormatter() = YearValueFormatter(0)

}
package info.fandroid.ext

import android.util.Log
import info.fandroid.dowcryptsdcr.model.RealmIndex
import io.realm.Realm
import io.realm.RealmChangeListener
import io.realm.RealmObject
import java.util.*

const val LAST_DATABASE_DATE = 1548194400000

fun Realm.saveRealmObject(obj: RealmObject) {
    beginTransaction()
    insertOrUpdate(obj)
    commitTransaction()
}


fun Realm.saveRealmObjects(objects: List<RealmObject>) {
    executeTransaction { insertOrUpdate(objects) }
}

fun Realm.saveRealmObjectsWithListener(objects: List<RealmObject>, listener: ((Realm)->Unit)) {
    addChangeListener(listener)
    beginTransaction()
    insertOrUpdate(objects)
    commitTransaction()
    removeChangeListener(listener)
}


fun Realm.addRealmObjects(objects: List<RealmObject>) {
    beginTransaction()
    insert(objects)
    commitTransaction()
}



fun Realm.findMissingDates(calendar: Calendar = Calendar.getInstance()): List<String> {
    //val last = where(RealmIndex::class.java).findAll().last()

    val missingDates = ArrayList<String>()

    var from = LAST_DATABASE_DATE - WEEK_IN_MILLIS - WEEK_IN_MILLIS - WEEK_IN_MILLIS - WEEK_IN_MILLIS
    val to = calendar.timeInMillis + WEEK_IN_MILLIS

//    for (i in from..to step WEEK_IN_MILLIS.toLong()) {
//        missingDates.add(i.dateToString(WEEK_DATE_PATTERN))
//    }

    Log.d("helloi", "from: ${from.dateToString()}, to: ${to.dateToString()}")

    while(from < to) {
        val miss = from.dateToString(WEEK_DATE_PATTERN)
        if (!missingDates.contains(miss)) {
            missingDates.add(miss)
        }

        from += WEEK_IN_MILLIS
    }

    return missingDates
}


fun Realm.findMissingDates2(calendar: Calendar = Calendar.getInstance()): List<String> {
    val last = where(RealmIndex::class.java).findAll().last()

    val missingDates = ArrayList<String>()

    var from = LAST_DATABASE_DATE - WEEK_IN_MILLIS - WEEK_IN_MILLIS - WEEK_IN_MILLIS - WEEK_IN_MILLIS
    val to = calendar.timeInMillis

//    for (i in from..to step WEEK_IN_MILLIS.toLong()) {
//        missingDates.add(i.dateToString(WEEK_DATE_PATTERN))
//    }

    Log.d("helloi", "from: ${from.dateToString()}, to: ${to.dateToString()}")

    while(from < to) {
        val miss = from.dateToString(WEEK_DATE_PATTERN)
        if (!missingDates.contains(miss)) {
            missingDates.add(miss)
        }

        from += WEEK_IN_MILLIS
    }

    return missingDates
}


fun Realm.getLastDate(): String {
    val last = where(RealmIndex::class.java).findAll().last()
    return last?.date!!
}
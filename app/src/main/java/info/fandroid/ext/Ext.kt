package info.fandroid.ext

import io.reactivex.Observable
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.SimpleDateFormat
import java.util.*
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import info.fandroid.ext.MapWithIndex.Indexed
import info.fandroid.dowcryptsdcr.model.RealmIndex
import kotlin.collections.ArrayList


const val DAY_IN_MILLIS = 86400000
const val WEEK_IN_MILLIS = 604800000

const val WEEK_DATE_PATTERN = "_yyyy_w"

fun Number.formatThousands(): String {
    val sb = StringBuilder()
    val formatter = Formatter(sb, Locale.US)
    formatter.format("$%(,.0f", this)
    return sb.toString()
}

fun String.usToEuDate(): String {
    val arr = split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    return arr[1] + "/" + arr[0] + "/" + arr[2]
}

fun String.euToUsDate(): String {
    val arr = split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    return arr[1] + "/" + arr[0] + "/" + arr[2]
}

fun String.formatDate(): String {
    val arr = split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    return arr[0] + "/" + arr[1] + "/" + arr[2]
}

fun String.toDate(): Date {
    return SimpleDateFormat("d/M/yyyy").parse(this)
}




fun String.toCalendar(): Calendar {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this.toDate().time
    return calendar
}


fun Number.dateToString(pattern: String = "d/M/yyyy"): String {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this.toLong()
    return SimpleDateFormat(pattern).format(calendar.time)
}


fun String.removeLast(): String {
    if (this.isNotEmpty()) {
        return this.substring(0, this.length - 1)
    }
    return this
}

fun Calendar.monthYear(): String {
    return "${this.get(Calendar.MONTH) + 1}/${this.get(Calendar.YEAR)}"
}

fun Calendar.weekMonthYear(): String {
    return "${this.get(Calendar.WEEK_OF_MONTH) + 1}/${this.get(Calendar.MONTH) + 1}/${this.get(Calendar.YEAR)}"
}

fun Calendar.dayMonthYear(): String {
    return "${this.get(Calendar.DAY_OF_MONTH) + 1}/${this.get(Calendar.MONTH) + 1}/${this.get(Calendar.YEAR)}"
}


fun Double.round(places: Int): Double {
    if (places < 0) throw IllegalArgumentException()

    var bd = BigDecimal(this)
    bd = bd.setScale(places, RoundingMode.HALF_UP)
    return bd.toDouble()
}



fun String.stringToDate(pattern: String): Date {
    return SimpleDateFormat(pattern).parse(this)
}

fun Date.dateToString(pattern: String): String {
    return SimpleDateFormat(pattern).format(this)
}


fun String.stringToDate(): Date {
    return SimpleDateFormat("d/M/yyyy").parse(this)
}

fun Date.dateToString(): String {
    return SimpleDateFormat("d/M/yyyy").format(this)
}


fun Observable<Float>.sumFloat() = reduce { total, next -> total + next }

fun Observable<List<Float>?>.sumArray() = reduce { total, next -> total.zip(next) { xv, yv -> xv + yv } }
fun Observable<List<Float>?>.divideArray(by: Int) = reduce { total, next -> total.zip(next) { xv, yv -> xv + yv } }


fun <T> Observable<T>.mapWithIndex(): Observable<MapWithIndex.Indexed<T>> {
    return this.zipWith(MapWithIndex.NaturalNumbers.instance()) { value, index -> Indexed(value, index) }

}


fun Calendar.minusWeek() {
    timeInMillis -= WEEK_IN_MILLIS
}


fun FirebaseRemoteConfig.getWeek(week: String): List<Pair<String, Double>> {
    return getString(week).parseFirebaseWeek()
}

fun String.parseFirebaseWeek(): List<Pair<String, Double>> {
    val indexes = ArrayList<Pair<String, Double>>()

    val days = this.parseFirebaseDays()

    for (day in days) {
        val parsedDay = day.parseFirebaseDayIndex()

        val index = Pair(parsedDay[0], parsedDay[1].toDouble())

        indexes.add(index)
    }

    return indexes
}

fun FirebaseRemoteConfig.getRating(): List<String> {
    val rating = getString("rating")
    return rating.parseRating().toList()
}

fun String.parseFirebaseDays(): Array<String> {
    return split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
}

fun String.parseFirebaseDayIndex(): Array<String> {
    return split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
}

fun String.parseRating(): Array<String> {
    return split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
}





